FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder
WORKDIR /usr/src/app
COPY . .
RUN cargo lambda build --release --arm64
FROM public.ecr.aws/lambda/provided:al2-arm64
WORKDIR /fh89-week-10
COPY --from=builder /usr/src/app/target/ ./ 
COPY --from=builder /usr/src/app/src/pythia-410m-q4_0-ggjt.bin ./ 
RUN if [ -d /fh89-week-10/lambda/ids721-mini10/ ]; then echo "Directory exists"; else echo "Directory does not exist"; fi
RUN if [ -f /fh89-week-10/lambda/ids721-mini10/bootstrap ]; then echo "File exists"; else echo "File does not exist"; fi
ENTRYPOINT ["/fh89-week-10/lambda/ids721-mini10/bootstrap"]
