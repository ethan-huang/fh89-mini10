# IDS 721 Mini Project 10

This project demonstrates the process of dockerize transformer, deploy container to AWS Lambda and implement query endpoint by the use of Rust.

## git push to gitlab

Since the bin file is too larget, I have ignore the `pythia-410m-q4_0-ggjt.bin` file.

## Build Process:

Follow these steps to run your project:

### Step 1: Project Setup

1. **Create a New Rust Project:**
   Create a new Project using the following command:

   ```
   cargo lambda new ids721mini10
   ```

   Add dependencies to cargo.toml:

   ```
    [dependencies]
    lambda_http = "0.11.1"
    tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
    tracing = "0.1.27"
    log = "0.4.14"
    llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
    openssl = { version = "0.10.35", features = ["vendored"] }
    serde = {version = "1.0", features = ["derive"] }
    serde_json = "1.0"
    rand = "0.8.5"
   ```

2. **Add Functionalities:**
   Add codes in main.rs to add in functionalities. Also, include the model file in the project, which is `pythia-410m-q4_0-ggjt.bin` in my case.

3. **Test your project locally:**
   Run the following command to test your code locally:
   `cargo lambda watch` and `curl http://localhost:8080/\?text = hello%20world `

#### Containarize the Project:

1. Create a new private repository in AWS ECR.
   ![Alt text](/image/repo.png)
2. Retrieve an authentication token and authenticate your Docker client to your registry using the following command: `aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <user-id>.dkr.ecr.us-east-1.amazonaws.com`
3. Build your Docker image using the following command: `docker build -t <project> .`
4. After the build completes, tag your image so you can push the image to this repository: `docker tag <image>:latest <aws id>.dkr.ecr.us-east-1.amazonaws.com/<image>:latest`
5. Run the following command to push this image to your newly created AWS repository: `docker push <aws id>.dkr.ecr.us-east-1.amazonaws.com/<image>:latest` Then we can see that the image is pushed to ECR repo:
   ![Alt text](/image/ECR.png)

#### AWS Lambda Function:

1. Create a new Lambda function using a container image. Remember to choose the image you pushed to ECR.
2. Create a new function URL enabling CORS. Adjust the memory size and timeout length accordingly so that the model can reply.

- Navigate to your function's configuration page.
- Select "Function URL" and click "Create function URL".
- Choose the appropriate method (POST for inference requests).
- Enable CORS by adding \* to "Allow origin" for testing, or specify your domain for production.

3. Create a function URL for the lambda function for testing.
   ![Alt text](/image/lambda.png)

Use `curl https://4igj3f7dltrapicicm5r6df4tq0vsvwq.lambda-url.us-east-1.on.aws/\?text=hello%20world` to check the results:

![Alt text](/image/result.png)
